#!/usr/bin/env nextflow

process somalier_extract {
// Runs MarkDuplicates
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: mkdup_bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*.mkdup.bam") - MarkDup Alignment File
//
// require:
//   BAMS
//   params.picard_mark_duplicates_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'somalier_container'
  label 'somalier_extract'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam), path(bai)
  path(ref)
  path(known_sites)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*somalier_extract"), emit: somalier_extracts

  script:
  """
  somalier extract \
  -d ${dataset}-${pat_name}-${run}.somalier_extract \
  --sites ${known_sites} \
  -f ${ref} \
  ${parstr} \
  ${bam}
  """
}

process somalier_relate {
// Runs MarkDuplicates
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: mkdup_bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*.mkdup.bam") - MarkDup Alignment File
//
// require:
//   BAMS
//   params.picard_mark_duplicates_parameters

  tag "${dataset}/${pat_name}"
  label 'somalier_container'
  label 'somalier_relate'
  cache 'lenient'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/somalier"

  input:
  tuple val(pat_name), val(dataset), path(bams)
  val parstr

  output:
  tuple val(pat_name), val(dataset), path("${dataset}-${pat_name}.groups.tsv"), emit: somalier_groups
  tuple val(pat_name), val(dataset), path("${dataset}-${pat_name}.pairs.tsv"), emit: somalier_pairs
  tuple val(pat_name), val(dataset), path("${dataset}-${pat_name}.samples.tsv"), emit: somalier_samples

  script:
  """
  FILE_LIST=`ls *extract/*`
  somalier relate \${FILE_LIST} \
  -o ${dataset}-${pat_name}
  ${parstr}
  """
}
